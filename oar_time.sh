#!/bin/bash
set -euo pipefail

# Format seconds to a human-readable format
function format_seconds {
        if [ $1 -le 60 ]; then
                echo "$1s"
        elif [ $1 -le 3600 ]; then
                echo "$(($1 / 60))m"
        else
                echo "$(($1 / 3600))h$((($1 / 60) % 60))m"
        fi
}

# Converts a walltime to seconds
function time_to_seconds {
        IFS=: read hours mins secs <<< $1

        echo $((hours * 3600 + mins * 60 + secs))
}

oarstat -u -J |
        jq --raw-output 'to_entries |
                 map(select(.value.state == "Running")) |
                 map("\(.key),\(.value.startTime)") |
                 join("\n")' |
        while IFS=$',' read -r oar_job_id start_time; do
                walltime=$(oarwalltime $oar_job_id | grep "Current walltime" | cut -d: -f2,3,4 | xargs)
                walltime_secs=$(time_to_seconds $walltime)
                end_date=$(( $start_time + $walltime_secs))
                remaining_seconds=$(( $end_date - $(date +%s) ))
                formatted_time="$(format_seconds $remaining_seconds)"
                echo "$oar_job_id $end_date $formatted_time"
        done |
        sort -k 2 |
        head -n 1 |
        awk 'END{print (NF == 3 && NR > 0) ? $1 " -> " $3 : "no job running"}'

