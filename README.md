# OAR Time

Grid'5000 utility to display current OAR job & remaining time

## Integration in TMUX

In your `~/.tmux.conf` (create it if does not exist) you can add a call to the script in the status line:

```
set -g status-right-length "100" # just to make sure everything fits on screen
set-option -g status-right "[#(sh $HOME/office-431/oar-time/oar_time.sh)] \"#h\" %H:%M %d-%m-%Y"
```

to update: `tmux source-file ~/.tmux.conf`
